FROM amazoncorretto:11
EXPOSE 9001
ADD target/cloud-api-gateway.jar cloud-api-gateway.jar
ENTRYPOINT ["java","-jar","/cloud-api-gateway.jar"]