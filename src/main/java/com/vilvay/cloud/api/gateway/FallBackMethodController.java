package com.vilvay.cloud.api.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackMethodController {

    @GetMapping("/authServiceFallBack")
    public String authServiceFallBackServiceMethod(){
        return "Auth service is taking longer than expected." +
                " Please Try again later.";
    }

    @GetMapping("/bloggerServiceFallBack")
    public String bloggerServiceFallBackServiceMethod(){
        return "Blogger service is taking longer than expected." +
                " Please Try again later.";
    }
}
